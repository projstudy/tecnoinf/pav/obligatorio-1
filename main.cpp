/**
 * Tecnologo Nocturno Latu - 2019
 *
 * Franco Rodriguez  4.684.572-7
 * Juan Mussini      4.442.755-3
 * Mathias Castro    4.523.221-8
 * Mariano Zunino    4.974.616-4
 *
 * */

#include "main.h"
#include "Clases/Gato.h"
#include "Clases/Perro.h"
#include "Clases/Socio.h"
#include "DataTypes/DtConsulta.h"
#include "DataTypes/DtGato.h"
#include "DataTypes/DtMascota.h"
#include "DataTypes/DtPerro.h"
#include "ui.h"
#include <cstdlib>
#include <iostream>
#include <stdexcept>
#include <stdlib.h>
#include <typeinfo>

#define MAX_SOCIOS 100

using namespace std;

struct socios {
  Socio *socios[MAX_SOCIOS];
  int tope;
} coleccionSocios;

int main() {
  menu();
  return 0;
}

// FUNCION AUXILIAR BUSCAR SOCIO
Socio *buscarSocio(string ci) {
  Socio *socio = NULL;
  int indice = 0;
  while (indice < coleccionSocios.tope && socio == NULL) {
    if (coleccionSocios.socios[indice]->getCi() == ci) {
      socio = coleccionSocios.socios[indice];
    }
    indice++;
  }
  return socio;
}

// FUNCION A
void registrarSocio(string ci, string nombre, DtMascota &dtMascota) {
  if (coleccionSocios.tope == MAX_SOCIOS) {
    throw invalid_argument("Se llego a la maxima cantidad de socios.");
  } else {

    Socio *socio = new Socio(ci, nombre);
    coleccionSocios.socios[coleccionSocios.tope] = socio;
    coleccionSocios.tope++;

    Mascota *mascota;

    try {
      DtPerro dtPerro = dynamic_cast<DtPerro &>(dtMascota);
      mascota =
          new Perro(dtPerro.getNombre(), dtPerro.getGenero(), dtPerro.getPeso(),
                    dtPerro.getRazaPerro(), dtPerro.esVacunaCachorro());
      socio->agregarMascota(mascota);
    } catch (bad_cast &e) {
    }
    //
    try {
      DtGato dtGato = dynamic_cast<DtGato &>(dtMascota);
      mascota = new Gato(dtGato.getNombre(), dtGato.getGenero(),
                         dtGato.getPeso(), dtGato.getTipoPelo());
      socio->agregarMascota(mascota);
    } catch (bad_cast &e) {
    }
  }
}

// FUNCION B
void agregarMascota(string ci, DtMascota &dtMascota) {
  Socio *socio = buscarSocio(ci);
  if (socio == NULL) {
    throw invalid_argument("No existe un socio registrado con esa cedula.");
  } else {
    Mascota *mascota;
    try {
      // Pruebo castear el dtMascota a DtPerro
      DtPerro dtPerro = dynamic_cast<DtPerro &>(dtMascota);
      // Si funca, creo un perro con los datos del dtPerro
      mascota =
          new Perro(dtPerro.getNombre(), dtPerro.getGenero(), dtPerro.getPeso(),
                    dtPerro.getRazaPerro(), dtPerro.esVacunaCachorro());
      // agrego el nuevo perro/mascota al array de mascotas del socio
      socio->agregarMascota(mascota);
    } catch (bad_cast &e) {
      // no hacemos nada si falla el cast, probamos con el el Gato ahora
    }
    try {
      // misma historia
      DtGato dtGato = dynamic_cast<DtGato &>(dtMascota);
      mascota = new Gato(dtGato.getNombre(), dtGato.getGenero(),
                         dtGato.getPeso(), dtGato.getTipoPelo());
      socio->agregarMascota(mascota);
    } catch (bad_cast &e) {
      // no hacemos nada si falla el cast, deberia haber funcionado el del
      // Perro
    }
  }
}

// FUINCION C
void ingresarConsulta(string motivo, string ci /*, DtFecha x*/) {
  Consulta *con = new Consulta(DtFecha(), motivo);
  // Consulta *con = new Consulta(x, motivo);
  Socio *socio = buscarSocio(ci);
  if (socio == NULL) {
    throw invalid_argument("No existe un socio registrado con esa cedula.");
  } else {
    socio->agregarConsulta(con);
  }
}

// FUNCION D
DtConsulta **verConsultasAntesDeFecha(DtFecha &fecha, string ciSocio,
                                      int &cantConsultas) {
  Socio *socio = buscarSocio(ciSocio);
  if (socio == NULL) {
    throw invalid_argument("No existe un socio registrado con esa cedula.");
  } else {

    int topeConsultas;
    // obtengo el array de consultas y la cantidad q tiene (topeConsultas)
    Consulta **consultas = socio->obtenerConsultas(topeConsultas);
    if (cantConsultas > topeConsultas) {
      // si es necesario ajusto la cant de consultas al tope
      cantConsultas = topeConsultas;
    }
    DtConsulta **dtConsultas = new DtConsulta *[cantConsultas];
    int indice = 0;
    for (int i = 0; i < cantConsultas; i++) {
      if (consultas[i]->getFechaConsulta() < fecha) {
        // si consulta me sirve, la agrego al array, en posicion indice;
        dtConsultas[indice] = new DtConsulta(consultas[i]->getFechaConsulta(),
                                             consultas[i]->getMotivo());
        // incremento el indice;
        indice++;
      }
    }
    return dtConsultas;
  }
}

// FUNCION E
void eliminarSocio(string ci) {
  Socio *socio = buscarSocio(ci);
  if (socio == NULL) {
    throw invalid_argument("No existe un socio registrado con esa cedula.");
  } else {
    delete socio;
    coleccionSocios.tope--;
  }
}

// FUNCION F
DtMascota **obtenerMascotas(string ci, int &cantMascotas) {
  Socio *socio = buscarSocio(ci);
  if (socio == NULL) {
    throw invalid_argument("No existe un socio registrado con esa cedula.");
  } else {
    int topeMascotas;
    // Obtengo el array de mascotas
    Mascota **mascotas = socio->obtenerMascotas(topeMascotas);
    if (cantMascotas > topeMascotas) {
      // Corrijo la cantidad de ser necesario
      cantMascotas = topeMascotas;
    }
    // Creo el nuevo array con el tamaño justito
    DtMascota **dtMascotas = new DtMascota *[cantMascotas];
    for (int i = 0; i < cantMascotas; i++) {
      // Pruebo castear la mascota[i] a Perro
      Perro *perro = dynamic_cast<Perro *>(mascotas[i]);
      if (perro != NULL) {
        // Si funco, creo el DtPerro y lo mando pal array solucion
        dtMascotas[i] = new DtPerro(perro->getNombre(), perro->getGenero(),
                                    perro->getPeso(), perro->getRaza(),
                                    perro->esVacunaCachorro(),
                                    perro->obtenerRacionDiaria());
      } else {
        // Si estoy en este punto, es xq la mascota no es un Perro, y tiene
        // que ser si o si, un gato.
        Gato *gato = dynamic_cast<Gato *>(mascotas[i]);
        if (gato != NULL) {
          // El if no es necesario, es solo para que el linter no rompa las
          // bolas.
          dtMascotas[i] =
              new DtGato(gato->getNombre(), gato->getGenero(), gato->getPeso(),
                         gato->getTipoPelo(), gato->obtenerRacionDiaria());
        }
      }
    }
    return dtMascotas;
  }
}
