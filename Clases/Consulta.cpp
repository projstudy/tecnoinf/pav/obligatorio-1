#include "Consulta.h"
#include "../DataTypes/DtFecha.h"

// constructor x defecto
Consulta::Consulta() { this->motivo = ""; }

// constructor con parametros
Consulta::Consulta(DtFecha fechaConsulta, string motivo) {
  this->fechaConsulta = fechaConsulta;
  this->motivo = motivo;
}

// Setters y Getters

DtFecha Consulta::getFechaConsulta() { return fechaConsulta; }

void Consulta::setFechaConsulta(DtFecha fechaConsulta) {
  Consulta::fechaConsulta = fechaConsulta;
}

string Consulta::getMotivo() { return motivo; }

void Consulta::setMotivo(string motivo) { Consulta::motivo = motivo; }

Consulta::~Consulta() {}
