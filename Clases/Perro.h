#ifndef PERRO_H
#define PERRO_H
#include "../Tipos/RazaPerro.h"
#include "../Tipos/TipoPelo.h"
#include "Mascota.h"
#include <iostream>

using namespace std;

class Perro : public Mascota {
private:
  RazaPerro raza;
  bool vacunaCachorro;

public:
  /*Constructor por defecto*/
  Perro();
  /*Constructor por parametro*/
  Perro(string, Genero, float, RazaPerro, bool);

  /*Getters y Setters*/
  RazaPerro getRaza();
  void setRaza(RazaPerro);

  bool esVacunaCachorro();
  void setVacunaCachorro(bool);

  /*Destructor*/
  ~Perro();

  float obtenerRacionDiaria();
};

#endif // PERRO_H
