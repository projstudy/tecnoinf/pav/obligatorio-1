#ifndef CONSULTA_H
#define CONSULTA_H
#include "../DataTypes/DtFecha.h"
#include "Mascota.h"
#include <iostream>

using namespace std;

class Consulta {
private:
  DtFecha fechaConsulta;
  string motivo;

public:
  /*Constructor por defecto*/
  Consulta();

  /*Constructor por parametro*/
  Consulta(DtFecha, string);

  /*Getters y Setters*/
  DtFecha getFechaConsulta();

  void setFechaConsulta(DtFecha fechaConsulta);

  string getMotivo();

  void setMotivo(string motivo);

  /*Destructor*/
  ~Consulta();
};

#endif // CONSULTA_H
