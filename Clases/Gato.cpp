#include "Gato.h"
#include "Mascota.h"

// constructor x defecto
Gato::Gato() {}

// constructor con parametros
Gato::Gato(string nombre, Genero genero, float peso, TipoPelo tipoPelo)
    : Mascota(nombre, genero, peso) {
  this->tipoPelo = tipoPelo;
}

// Setters y Getters
TipoPelo Gato::getTipoPelo() { return this->tipoPelo; }
void Gato::setTipoPelo(TipoPelo tipoPelo) { this->tipoPelo = tipoPelo; }

// Operacion
float Gato::obtenerRacionDiaria() { return this->getPeso() * 0.015; }

Gato::~Gato() {}
