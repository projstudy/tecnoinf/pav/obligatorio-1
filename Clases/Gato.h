
#ifndef GATO_H
#define GATO_H

#include "../Tipos/TipoPelo.h"
#include "Mascota.h"
#include <iostream>

using namespace std;

class Gato : public Mascota {
private:
  TipoPelo tipoPelo;

public:
  /*Constructor por defecto*/
  Gato();
  /*Constructor por parametro*/
  Gato(string, Genero, float, TipoPelo);

  /*Getters y Setters*/
  TipoPelo getTipoPelo();
  void setTipoPelo(TipoPelo);

  /*Destructor*/
  ~Gato();

  float obtenerRacionDiaria();
};

#endif // GATO_H
