#include "Mascota.h"

// constructor x defecto
Mascota::Mascota() {
  this->nombre = "";
  this->peso = 0;
  this->racionDiaria = 0;
}

// constructor con parametros
Mascota::Mascota(string nombre, Genero genero, float peso) {
  this->nombre = nombre;
  this->genero = genero;
  this->peso = peso;
  this->racionDiaria = 0;
}

// Setters y Getters
string Mascota::getNombre() { return this->nombre; }
void Mascota::setNombre(string nombre) { this->nombre = nombre; }

Genero Mascota::getGenero() { return this->genero; }
void Mascota::setGenero(Genero genero) { this->genero = genero; }

float Mascota::getPeso() { return this->peso; }
void Mascota::setPeso(float peso) { this->peso = peso; }

// Destructor

Mascota::~Mascota() {}
