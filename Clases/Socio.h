#ifndef SOCIO_H
#define SOCIO_H

#include "../DataTypes/DtFecha.h"
#include "Consulta.h"
#include "Mascota.h"
#include <iostream>

using namespace std;

class Socio {
private:
  string ci;
  string nombre;
  DtFecha fechaIngreso;
  int topeMascota;
  Mascota *mascotas[10];
  int topeConsulta;
  Consulta *consultas[20];

public:
  /*Constructor por defecto*/
  Socio();

  /*Constructor por parametro*/
  Socio(string, string);

  /*Getters y Setters*/
  string getCi();

  void setCi(string ci);

  string getNombre();

  void setNombre(string nombre);

  DtFecha getFechaIngreso();

  void setFechaIngreso(DtFecha fechaIngreso);

  /*Opeacion*/
  void agregarMascota(Mascota *);

  void agregarConsulta(Consulta *);

  Consulta **obtenerConsultas(int &tope);

  Mascota **obtenerMascotas(int &cantMascotas);

  /*Destructor*/
  ~Socio();
};

#endif // SOCIO_H
