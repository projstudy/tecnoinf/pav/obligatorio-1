#include "Perro.h"
#include "Mascota.h"

// constructor x defecto
Perro::Perro() : Mascota() {}

// constructor con parametros
Perro::Perro(string nombre, Genero genero, float peso, RazaPerro raza,
             bool vacunaCachorro)
    : Mascota(nombre, genero, peso) {
  this->raza = raza;
  this->vacunaCachorro = vacunaCachorro;
}

// Setters y Getters
RazaPerro Perro::getRaza() { return this->raza; }
void Perro::setRaza(RazaPerro raza) { this->raza = raza; }

bool Perro::esVacunaCachorro() { return this->vacunaCachorro; }
void Perro::setVacunaCachorro(bool vacunaCachorro) {
  this->vacunaCachorro = vacunaCachorro;
};

// Operacion
float Perro::obtenerRacionDiaria() { return this->getPeso() * 0.025; }

Perro::~Perro() {}
