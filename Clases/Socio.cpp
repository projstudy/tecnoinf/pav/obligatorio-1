#include "Socio.h"
#include "../DataTypes/DtFecha.h"
#include <stdexcept>

// constructor x defecto
Socio::Socio() {
  this->ci = "";
  this->nombre = "";
  this->topeMascota = 0;
  this->topeConsulta = 0;
}

// constructor con parametros
Socio::Socio(string ci, string nombre) {
  this->ci = ci;
  this->nombre = nombre;
  // TODO: esto hay que cambiarlo, en espera de ver que dice Yona
  this->fechaIngreso = DtFecha();
  this->topeConsulta = 0;
  this->topeMascota = 0;
}

// Setters y Getters

string Socio::getCi() { return ci; }

void Socio::setCi(string ci) { Socio::ci = ci; }

string Socio::getNombre() { return nombre; }

void Socio::setNombre(string nombre) { Socio::nombre = nombre; }

DtFecha Socio::getFechaIngreso() { return fechaIngreso; }

void Socio::setFechaIngreso(DtFecha fechaIngreso) {
  Socio::fechaIngreso = fechaIngreso;
}

void Socio::agregarMascota(Mascota *mascota) {
  if (this->topeMascota == 10) {
    throw invalid_argument("Se llego al maximo de mascotas.");
  } else {
    this->mascotas[this->topeMascota] = mascota;
    this->topeMascota++;
  }
}

void Socio::agregarConsulta(Consulta *consulta) {
  if (this->topeConsulta == 20) {
    throw invalid_argument("Se llego al maximo de consultas.");
  } else {
    this->consultas[this->topeConsulta] = consulta;
    this->topeConsulta++;
  }
}

Consulta **Socio::obtenerConsultas(int &tope) {
  tope = this->topeConsulta;
  return this->consultas;
}

Mascota **Socio::obtenerMascotas(int &cantMascotas) {
  cantMascotas = this->topeMascota;
  return this->mascotas;
}

Socio::~Socio() {
  for (int i = 0; i < this->topeConsulta; ++i) {
    delete this->consultas[i];
  }

  for (int j = 0; j < this->topeMascota; ++j) {
    delete this->mascotas[j];
  }
}
