#ifndef MASCOTA_H
#define MASCOTA_H

#include "../Tipos/Genero.h"
#include <iostream>

using namespace std;

class Mascota {
private:
  string nombre;
  Genero genero;
  float peso;
  float racionDiaria;

public:
  /*Constructor por defecto*/
  Mascota();

  /*Constructor por parametro*/
  Mascota(string, Genero, float);

  /*Getters y Setters*/
  string getNombre();

  void setNombre(string);

  Genero getGenero();

  void setGenero(Genero);

  float getPeso();

  void setPeso(float);

  /*Operacion*/
  virtual float obtenerRacionDiaria() = 0;

  /*Destructor*/
  virtual ~Mascota() = 0;
};

#endif // MASCOTA_H
