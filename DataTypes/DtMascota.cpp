#include "DtMascota.h"
#include <iostream>

using namespace std;

DtMascota::DtMascota(){};

DtMascota::DtMascota(string nombre, Genero genero, float peso,
                     float racionDiaria) {
  this->nombre = nombre;
  this->genero = genero;
  this->peso = peso;
  this->racionDiaria = racionDiaria;
}

string DtMascota::getNombre() { return this->nombre; }

Genero DtMascota::getGenero() { return this->genero; }

float DtMascota::getPeso() { return this->peso; }

float DtMascota::getRacionDiaria() { return this->racionDiaria; }

DtMascota::~DtMascota() {}
