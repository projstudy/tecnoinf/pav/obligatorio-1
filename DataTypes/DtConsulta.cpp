#include "DtConsulta.h"
#include "DtFecha.h"
#include <iostream>

using namespace std;

DtConsulta::DtConsulta() {}

DtConsulta::DtConsulta(DtFecha fechaConsulta, string motivo) {
  this->fechaConsulta = fechaConsulta;
  this->motivo = motivo;
}

DtFecha DtConsulta::getFechaConsulta() { return this->fechaConsulta; }

string DtConsulta::getMotivo() { return this->motivo; }

DtConsulta::~DtConsulta() {}

ostream &operator<<(ostream &salida, DtConsulta &consulta) {
  // la consulta tiene una fecha, asi que tambien hago overload de dtFecha
  DtFecha fecha = consulta.getFechaConsulta();
  return salida << "- Fecha consulta: " << fecha
                << "- Motivo: " << consulta.getMotivo() << endl;
};
