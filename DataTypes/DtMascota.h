#ifndef DTMASCOTA_H
#define DTMASCOTA_H

#include "../Tipos/Genero.h"
#include <iostream>

using namespace std;

class DtMascota {
private:
  string nombre;
  Genero genero;
  float peso;
  float racionDiaria;

public:
  DtMascota();

  DtMascota(string nombre, Genero genero, float peso, float racionDiaria);

  string getNombre();

  Genero getGenero();

  float getPeso();

  float getRacionDiaria();

  virtual ~DtMascota() = 0;
};

#endif // DTMASCOTA_H
