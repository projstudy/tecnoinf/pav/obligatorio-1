#ifndef DTFECHA_H
#define DTFECHA_H

#include <iostream>

using namespace std;

class DtFecha {

  friend ostream &operator<<(ostream &salida, DtFecha &fecha);

  friend bool operator<(DtFecha, DtFecha);

private:
  int dia;
  int mes;
  int anio;

public:
  DtFecha();

  DtFecha(int, int, int);

  int getDia();

  int getMes();

  int getAnio();

  ~DtFecha();
};

#endif // DTFECHA_H
