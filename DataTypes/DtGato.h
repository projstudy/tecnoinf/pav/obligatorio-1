#ifndef DTGATO_H
#define DTGATO_H

using namespace std;

#include "../Tipos/Genero.h"
#include "../Tipos/TipoPelo.h"
#include "DtMascota.h"
#include <iostream>

class DtGato : public DtMascota {
  friend ostream &operator<<(ostream &, DtGato &);

private:
  TipoPelo tipoPelo;

public:
  DtGato();

  DtGato(string, Genero, float, TipoPelo, float = 0);

  TipoPelo getTipoPelo();

  ~DtGato();
};

#endif // DTGATO_H
