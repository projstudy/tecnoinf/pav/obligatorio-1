#ifndef DTPERRO_H
#define DTPERRO_H

#include "../Tipos/Genero.h"
#include "../Tipos/RazaPerro.h"
#include "DtMascota.h"
#include <iostream>

using namespace std;

class DtPerro : public DtMascota {
  friend ostream &operator<<(ostream &, DtPerro &);

private:
  RazaPerro raza;
  bool vacunaCachorro;

public:
  DtPerro();

  DtPerro(string, Genero, float, RazaPerro, bool, float = 0);

  RazaPerro getRazaPerro();

  bool esVacunaCachorro();

  ~DtPerro();
};

#endif // DTPERRO_H
