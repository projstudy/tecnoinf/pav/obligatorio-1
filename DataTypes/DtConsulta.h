#ifndef DTCONSULTA_H
#define DTCONSULTA_H

#include "DtFecha.h"
#include <iostream>

using namespace std;

class DtConsulta {
  friend ostream &operator<<(ostream &salida, DtConsulta &consulta);

private:
  DtFecha fechaConsulta;
  string motivo;

public:
  DtConsulta();

  DtConsulta(DtFecha, string);

  DtFecha getFechaConsulta();

  string getMotivo();

  ~DtConsulta();
};

#endif // DTCONSULTA_H
