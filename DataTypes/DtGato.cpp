#include "DtGato.h"
#include "../Tipos/Genero.h"
#include "../Tipos/TipoPelo.h"
#include "DtMascota.h"
#include <iostream>

using namespace std;

DtGato::DtGato(string nombre, Genero genero, float peso, TipoPelo tipoPelo,
               float racionDiaria)
    : DtMascota(nombre, genero, peso, racionDiaria) {
  this->tipoPelo = tipoPelo;
}

TipoPelo DtGato::getTipoPelo() { return this->tipoPelo; }

DtGato::~DtGato() {}

ostream &operator<<(ostream &salida, DtGato &gato) {
  string genero = "Macho";
  if (gato.getGenero() == Hembra) {
    genero = "Hembra";
  }

  string pelo = "Corto";
  if (gato.getTipoPelo() == Mediano) {
    pelo = "Mediano";
  } else if (gato.getTipoPelo() == Largo) {
    pelo = "Largo";
  }

  return salida << "- Nombre: " << gato.getNombre() << "\n- Genero: " << genero
                << "\n- Peso: " << gato.getPeso() << " kg"
                << "\n- Racion diaria: " << gato.getRacionDiaria() << " kg"
                << "\n- Tipo de pelo: " << pelo;
};
