#include "DtPerro.h"
#include "../Tipos/Genero.h"
#include "../Tipos/RazaPerro.h"
#include "DtMascota.h"
#include <iostream>

using namespace std;

DtPerro::DtPerro(){};

DtPerro::DtPerro(string nombre, Genero genero, float peso, RazaPerro raza,
                 bool vacunaCachorro, float racionDiaria)
    : DtMascota(nombre, genero, peso, racionDiaria) {
  this->raza = raza;
  this->vacunaCachorro = vacunaCachorro;
};

RazaPerro DtPerro::getRazaPerro() { return this->raza; }

bool DtPerro::esVacunaCachorro() { return this->vacunaCachorro; }

DtPerro::~DtPerro() {}

ostream &operator<<(ostream &salida, DtPerro &perro) {
  string genero = "Macho";
  if (perro.getGenero() == Hembra) {
    genero = "Hembra";
  }
  string vacunado = "No";
  if (perro.esVacunaCachorro()) {
    vacunado = "Si";
  }

  string raza;
  switch (perro.getRazaPerro()) {
  case 0:
    raza = "Labrador";
    break;
  case 1:
    raza = "Ovejero";

    break;
  case 2:
    raza = "Bulldog";

    break;
  case 3:
    raza = "Pitbull";

    break;
  case 4:
    raza = "Collie";

    break;
  case 5:
    raza = "Pekines";

    break;
  default:
    raza = "Otro";
    break;
  }

  return salida << "- Nombre: " << perro.getNombre() << "\n- Genero: " << genero
                << "\n- Peso: " << perro.getPeso() << " kg"
                << "\n- Racion diaria: " << perro.getRacionDiaria() << " kg"
                << "\n- Raza: " << raza
                << "\n- Tiene vacuna del Cachorro: " << vacunado;
};
