#include "DtFecha.h"
#include <iostream>
#include <stdexcept>
#include <time.h>

using namespace std;

DtFecha::DtFecha() {
  time_t today = time(NULL);
  tm *now = localtime(&today);
  this->dia = now->tm_mday;
  this->mes = now->tm_mon + 1;
  this->anio = now->tm_year + 1900;
}

DtFecha::DtFecha(int dia, int mes, int anio) {
  if (dia < 1 || dia > 31)
    throw invalid_argument("El día no es válido.");

  if (mes < 1 || mes > 12)
    throw invalid_argument("El mes no es válido.");

  if (anio < 1900)
    throw invalid_argument("El año no es válido.");

  this->dia = dia;
  this->mes = mes;
  this->anio = anio;
}

int DtFecha::getDia() { return this->dia; }

int DtFecha::getMes() { return this->mes; }

int DtFecha::getAnio() { return this->anio; }

DtFecha::~DtFecha() {}

bool operator<(DtFecha f1, DtFecha f2) {
  bool esMenor = false;
  if (f1.getAnio() < f2.getAnio()) {
    esMenor = true;
  }
  if (f1.getAnio() == f2.getAnio()) {
    esMenor = f1.getMes() < f2.getMes();
  }
  if (f1.getAnio() == f2.getAnio() && f1.getMes() == f2.getMes()) {
    esMenor = f1.getDia() < f2.getDia();
  }
  return esMenor;
}

ostream &operator<<(ostream &salida, DtFecha &fecha) {
  return salida << fecha.getDia() << "/" << fecha.getMes() << "/"
                << fecha.getAnio() << endl;
};
