#include "DataTypes/DtConsulta.h"
#include "DataTypes/DtGato.h"
#include "DataTypes/DtPerro.h"
#include "Tipos/RazaPerro.h"
#include "Tipos/TipoPelo.h"
#include "main.h"
#include "ui.h"
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <stdexcept>

int leerOpcion(int minimo, int maximo) {
  bool valido = false;
  int opcion;
  do {
    cin >> opcion;
    if (cin.fail() || opcion < minimo || opcion > maximo) {
      cin.clear();
      cin.ignore();
      cout << "Error, opcion incorrecta, intente nuevamente." << endl;
    } else {
      valido = true;
    }
  } while (!valido);
  return opcion;
};

void menu() {
  bool salir = false;
  while (salir == false) {
    int numero;
    clearScreen();
    cout << "*****************************************" << endl;
    cout << "   Sistema de la Veterinaria" << endl;
    cout << "               MENU           " << endl;
    cout << "Ingrese el numero de la opcion deseada \n" << endl;
    cout << "1- Registrar a un Socio." << endl;
    cout << "2- Agregar nueva Mascota a un Socio." << endl;
    cout << "3- Ingresar una consulta." << endl;
    cout << "4- Ver consultas antes de una fecha." << endl;
    cout << "5- Eliminar un Socio." << endl;
    cout << "6- Ver Mascotas de un Socio." << endl;
    cout << "0- Salir." << endl;
    cout << "*****************************************" << endl;
    cout << "> ";
    numero = leerOpcion(0, 6);
    clearScreen();
    string ci;
    string nombre;
    string nombreMascota;
    Genero genero;
    float peso;

    // Registrar Socio
    if (numero == 1) {

      cout << "REGISTRO DE SOCIO:" << endl;
      cout << "Ci del Socio: " << endl;
      cin >> ci;
      cout << "Nombre del Socio: " << endl;
      cin.ignore();
      getline(cin, nombre);
      cout << "Nombre mascota: " << endl;
      cin.ignore();
      getline(cin, nombreMascota);
      cout << "Genero: " << endl;
      cout << "1- Macho " << endl;
      cout << "2- Hembra " << endl;
      int gen = leerOpcion(1, 2);
      gen--;
      genero = static_cast<Genero>(gen);
      cout << "Peso: " << endl;
      cin >> peso;
      cout << "Tipo Mascota: " << endl;
      cout << "1- Perro " << endl;
      cout << "2- Gato" << endl;
      int pog = leerOpcion(1, 2);
      if (pog == 1) {
        // PERRO
        RazaPerro raza;
        int numRaza;
        cout << "raza: " << endl;
        cout << "1- Labrador " << endl;
        cout << "2- Ovejero Aleman" << endl;
        cout << "3- Bulldog" << endl;
        cout << "4- Pitbull" << endl;
        cout << "5- Collie" << endl;
        cout << "6- Pekines" << endl;
        cout << "7- Otro" << endl;
        numRaza = leerOpcion(1, 7);
        numRaza--;
        raza = (RazaPerro)numRaza;
        cout << "Vacunado: " << endl;
        cout << "1- Si " << endl;
        cout << "2- No " << endl;
        int v;
        cin >> v;
        bool vacuna;
        if (v == 1) {
          vacuna = true;
        } else {
          vacuna = false;
        }
        DtPerro masc = DtPerro(nombreMascota, genero, peso, raza,
                               vacuna); // Crea la mascota del socio
        try {
          registrarSocio(ci, nombre, masc);
          std::cout << "Socio ingresado." << std::endl;
        } catch (invalid_argument &error) {
          cout << "[ERROR] " << error.what() << endl;
        }
      } else {
        // GATO
        cout << "Tipo de Pelo: " << endl;
        cout << "1- Corto " << endl;
        cout << "2- Mediano " << endl;
        cout << "3- Largo " << endl;
        TipoPelo pelo;
        int numPelo;
        numPelo = leerOpcion(1, 3);
        numPelo--;
        pelo = (TipoPelo)numPelo;

        DtGato masc = DtGato(nombreMascota, genero, peso, pelo, 0);
        try {
          std::cout << "Socio ingresado." << std::endl;
          registrarSocio(ci, nombre, masc);
          pressEnter();
        } catch (invalid_argument &error) {
          cout << "[ERROR] " << error.what() << endl;
          pressEnter();
        }
      }
    }
    // Agregar Mascota
    if (numero == 2) {
      cout << "REGISTRO DE MASCOTA" << endl;
      cout << "Cedula del dueño de la mascota: " << endl;
      cin >> ci;
      cout << "Nombre mascota: " << endl;
      cin.ignore();
      getline(cin, nombreMascota);
      cout << "Genero: " << endl;
      cout << "1- Macho " << endl;
      cout << "2- Hembra" << endl;
      int gen = leerOpcion(1, 2);
      gen--;
      genero = static_cast<Genero>(gen);
      cout << "Peso: " << endl;
      cin >> peso;
      cout << "Tipo Mascota: " << endl;
      cout << "1- Perro " << endl;
      cout << "2- Gato" << endl;
      int pog = leerOpcion(1, 2);
      if (pog == 1) {
        // PERRO
        RazaPerro raza;
        int numRaza;
        cout << "raza: " << endl;
        cout << "1- Labrador " << endl;
        cout << "2- Ovejero Aleman" << endl;
        cout << "3- Bulldog" << endl;
        cout << "4- Pitbull" << endl;
        cout << "5- Collie" << endl;
        cout << "6- Pekines" << endl;
        cout << "7- Otro" << endl;
        numRaza = leerOpcion(1, 7);
        numRaza--;
        raza = static_cast<RazaPerro>(numRaza);
        cout << "Vacunado: " << endl;
        cout << "1- Si " << endl;
        cout << "2- No " << endl;
        int v;
        cin >> v;
        bool vacuna;
        if (v == 1) {
          vacuna = true;
        } else {
          vacuna = false;
        }
        DtPerro masc = DtPerro(nombreMascota, genero, peso, raza,
                               vacuna); // Crea la mascota del socio
        try {
          agregarMascota(ci, masc);
          cout << "Se ingreso mascota de forma correcta." << endl;
          pressEnter();
        } catch (invalid_argument &error) {
          cout << "[ERROR] " << error.what() << endl;
          pressEnter();
        }
      } else {
        // GATO
        cout << "Tipo de Pelo: " << endl;
        cout << "1- Corto " << endl;
        cout << "2- Mediano " << endl;
        cout << "3- Largo " << endl;
        TipoPelo pelo;
        int numPelo;
        numPelo = leerOpcion(1, 3);
        numPelo--;
        pelo = static_cast<TipoPelo>(numPelo);

        DtGato masc = DtGato(nombreMascota, genero, peso, pelo, 0);
        try {
          agregarMascota(ci, masc);
          cout << "Se ingreso mascota de forma correcta." << endl;
          pressEnter();
        } catch (invalid_argument &error) {
          cout << "[ERROR] " << error.what() << endl;
          pressEnter();
        }
      }
    }

    // Ingresar COnsulta
    if (numero == 3) {
      cout << "INGRESAR UNA CONSULTA \n" << endl;
      cout << "Ingrese motivo de la consulta:" << endl;
      string motivo;
      cin.ignore();
      getline(cin, motivo);
      cout << "Ingrese ci del socio:" << endl;
      cin >> ci;
      try {
        ingresarConsulta(motivo, ci);
        cout << "Consulta ingresada con exito." << endl;
        pressEnter();
      } catch (invalid_argument &error) {
        cout << "[ERROR] " << error.what() << endl;
        pressEnter();
      }
    }

    // Ver consultas anteriores a fecha
    if (numero == 4) {
      int dia;
      int mes;
      int anio;
      cout << "CONSULTAS ANTERIORES A UNA FECHA \n" << endl;
      cout << "Ingresar fecha:" << endl;
      cout << "Ingresar dia:" << endl;
      cin >> dia;
      cout << "Ingresar mes:" << endl;
      cin >> mes;
      cout << "Ingresar anio:" << endl;
      cin >> anio;
      cout << "Ingresar ci del socio:" << endl;
      cin >> ci;
      cout << "Ingrese la cantidad de consultas que desea listar [1-20]:"
           << endl;
      int cantConsultas;
      cantConsultas = leerOpcion(1, 20);
      try {
        DtFecha fecha = DtFecha(dia, mes, anio);
        DtConsulta **consultas =
            verConsultasAntesDeFecha(fecha, ci, cantConsultas);
        cout << "Cantidad de consultas: " << cantConsultas << endl << endl;
        for (int i = 0; i < cantConsultas; ++i) {
          // imprimo cada consulta haciendo uso del overload
          cout << *consultas[i] << endl;
        }
        pressEnter();
      } catch (invalid_argument &error) {
        cout << "[ERROR] " << error.what() << endl;
        pressEnter();
      }
    }

    // Eliminar Socio
    if (numero == 5) {
      cout << "ELIMINAR UN SOCIO" << endl;
      cout << "Ingresar ci del socio a eliminar:" << endl;
      cin >> ci;
      try {
        eliminarSocio(ci);
        cout << "Socio eliminado." << endl;
        pressEnter();
      } catch (invalid_argument &error) {
        cout << "[ERROR] " << error.what() << endl;
        pressEnter();
      }
    }

    // Ver Mascotas
    if (numero == 6) {
      cout << "VER MASCOTAS DE UN SOCIO" << endl;
      cout << "Ingresar ci del socio:" << endl;
      cin >> ci;
      cout << "Ingrese la cantidad de mascotas que desea listar [1-10]:"
           << endl;
      int cantMascotas;
      cantMascotas = leerOpcion(1, 10);
      try {
        // obtengo el array de DtMascota
        DtMascota **mascotas = obtenerMascotas(ci, cantMascotas);
        cout << "Cantidad de mascotas: " << cantMascotas << endl << endl;
        for (int i = 0; i < cantMascotas; ++i) {
          // Recorro todas las dtMascotas,
          // Por cada una tengo q ver si es un perro o gato, y luego
          // imprimirla haciendo uso del overload del cout
          DtPerro *dtPerro = dynamic_cast<DtPerro *>(mascotas[i]);
          if (dtPerro == NULL) {
            DtGato *dtGato = dynamic_cast<DtGato *>(mascotas[i]);
            cout << *dtGato << endl << endl;
          } else {
            cout << *dtPerro << endl << endl;
          }
        }
        pressEnter();
      } catch (invalid_argument &error) {
        cout << "[ERROR] " << error.what() << endl;
        pressEnter();
      }
    }

    // SALIR
    if (numero == 0) {
      cout << "HASTA PRONTO..." << endl;
      salir = true;
    }
  }
}

void clearScreen() { // printf("\033[2J");
}

void pressEnter() {
  cout << "\nPresionar una tecla para continuar...\n";
  cin.ignore();
  cin.get();
  clearScreen();
}
