#ifndef MENU_H
#define MENU_H
#include "Clases/Socio.h"
#include "DataTypes/DtConsulta.h"
#include "DataTypes/DtMascota.h"
#include <iostream>

void registrarSocio(std::string ci, std::string nombre, DtMascota &dtMascota);
Socio *buscarSocio(string ci);
void agregarMascota(string ci, DtMascota &dtMascota);
void ingresarConsulta(string motivo, string ci /*, DtFecha x*/);
DtConsulta **verConsultasAntesDeFecha(DtFecha &fecha, string ciSocio,
                                      int &CantConsultas);
void eliminarSocio(string ci);
DtMascota **obtenerMascotas(string ci, int &cantMascotas);

#endif // MENU_H
