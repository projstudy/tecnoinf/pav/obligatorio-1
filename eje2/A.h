//
// Created by forbi on 3/24/19.
//

#ifndef EJE2_A_H
#define EJE2_A_H
class B;
class C;
class A {
private:
  B *b;
  C *c;
  int a;

public:
  // Constructor
  A();

  // Getters
  B *getB();
  C *getC();

  // Setters
  void setB(B *b);
  void setC(C *c);

  // Operaciones
  void info();

  // Destructor
  ~A();
};

#endif // EJE2_A_H
