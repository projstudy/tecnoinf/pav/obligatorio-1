//
// Created by forbi on 3/24/19.
//

#ifndef EJE2_C_H
#define EJE2_C_H

class B;
class A;
class C {
private:
  B *b;
  A *a;
  int c;

public:
  // Constructor
  C();

  // Getters
  B *getB();
  A *getA();

  // Setters
  void setB(B *b);
  void setA(A *a);

  // Operaciones
  void info();

  // Destructor
  ~C();
};

#endif // EJE2_C_H
