//
// Created by forbi on 3/24/19.
//

#include "C.h"
#include <iostream>

using std::cout;
using std::endl;

// Constructor
C::C() {
  this->a = nullptr;
  this->b = nullptr;
}

// Getters
A *C::getA() { return this->a; }

B *C::getB() { return this->b; }

// Setters
void C::setA(A *a) { this->a = a; }

void C::setB(B *b) { this->b = b; }

// Operaciones
void C::info() { cout << "Esta es la clase C" << endl; }

// Destructor
C::~C() = default;
