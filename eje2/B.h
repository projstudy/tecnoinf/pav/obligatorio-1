//
// Created by forbi on 3/24/19.
//

#ifndef EJE2_B_H
#define EJE2_B_H
class A;
class C;
class B {
private:
  A *a;
  C *c;
  int b;

public:
  // Constructor
  B();

  // Getters
  C *getC();
  A *getA();

  // Setters
  void setC(C *c);
  void setA(A *a);

  // Operaciones
  void info();

  // Destructor
  ~B();
};

#endif // EJE2_B_H
